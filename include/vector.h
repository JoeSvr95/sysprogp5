/* Definicion del tipo de datos vector3D */
typedef struct {
	float x;
	float y;
	float z;
} vector3D;

/* Funcion producto punto */
float dotproduct(vector3D,vector3D);

/*Funcion producto cruz*/
vector3D crossproduct(vector3D,vector3D);

/*Funcion para sacar la magnitud del vector*/
float magnitud(vector3D);

/*Verifica si un vector es ortogonal a otro*/
int esOrtogonal(vector3D,vector3D);

/* implementar aqu� las funciones requeridas */
#include<stdio.h>
#include<math.h>
#include<vector.h>

float dotproduct(vector3D v1, vector3D v2){
    return (v1.x*v2.x) + (v1.y*v2.y) + (v1.z*v2.z);
}

vector3D crossproduct(vector3D v1, vector3D v2){
	vector3D v3;
	v3.x= v1.y * v2.z - v1.z * v2.y;
	v3.y= v1.z * v2.x - v1.x * v2.z;
	v3.z= v1.x * v2.y - v1.y * v2.x;
	return v3; 
}

float magnitud(vector3D v){
    return sqrtf(powf(v.x,2) + powf(v.y,2) + powf(v.z,2));
}

int esOrtogonal(vector3D v1, vector3D v2){
    if (dotproduct(v1,v2) == 0)
        return 1;
    else
        return 0;
}
